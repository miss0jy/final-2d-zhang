﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class frog : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator Anim;
    private Collider2D Coll;
    public LayerMask Ground;
    public Transform leftpoint, rightpoint;
    private bool faceleft = true;
    public float Speed, JumpForce;
    public float leftx, rightx;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Anim = GetComponent<Animator>();
        Coll = GetComponent<Collider2D>();

        transform.DetachChildren();
        leftx = leftpoint.position.x;
        rightx = rightpoint.position.x;
        Destroy(leftpoint.gameObject);
        Destroy(rightpoint.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }
    void Movement()

    {
        if (faceleft)
        {

            if (Coll.IsTouchingLayers(Ground))
            {
                Anim.SetBool("Jumping", true);
                rb.velocity = new Vector2(-Speed, JumpForce);
            }
            if (transform.position.x < leftx)
            {
                transform.localScale = new Vector3(-1, 1, 1);
                faceleft = false;
            }
        }

        else
        {
            if (Coll.IsTouchingLayers(Ground))
            {
                Anim.SetBool("Jumping", true);
                rb.velocity = new Vector2(Speed, JumpForce);
            }
            if (transform.position.x > rightx)
            {
                transform.localScale = new Vector3(1, 1, 1);
                faceleft = true;
            }
        }
    }

    void SwitchAnim()
    {
        if (Anim.GetBool("Jumping"))
        {
            if (rb.velocity.y < 0.1)
            {
                Anim.SetBool("Jumping", false);
                Anim.SetBool("Falling", true);
            }
        }
        if (Coll.IsTouchingLayers(Ground) && Anim.GetBool("Falling"))
        {
            Anim.SetBool("Falling", false);
        }
            }
    void Death()
    {
        Destroy(gameObject);
    }
    public void JumpOn()
    {
        Anim.SetTrigger("death");
      
    }
}   
