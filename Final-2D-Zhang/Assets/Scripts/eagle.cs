﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eagle : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator Anim;
    private Collider2D Coll;
    public Transform Top, Bottom;
    private bool Up =true;
    public float Speed;
    public float Topy, Bottomy;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Anim = GetComponent<Animator>();
        Coll = GetComponent<Collider2D>();

        Topy = Top.position.y;
        Bottomy = Bottom.position.y;
        Destroy(Top.gameObject);
        Destroy(Bottom.gameObject);
    }

    void Update()
    {
        Movement();
    }
    void Movement()

    {
        if (Up)
        {
            rb.velocity = new Vector2(rb.velocity.x, Speed);
            if (transform.position.y > Topy)
            {
                Up = false;
            }
        }
        else
            rb.velocity = new Vector2(rb.velocity.x, -Speed);
        if (transform.position.y <Bottomy)
        {
            Up = true;
        }
    }
        }