﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Playermovement : MonoBehaviour
{
    public Rigidbody2D rb;
    public float speed;
    public float jumpforce;
    public Animator anim;
    public LayerMask ground;
    public Collider2D coll;
    public int Cherry = 0;
    public Text Cherrynum;
    public bool Hurting;

    void Start()
    {

    }


    void FixedUpdate()
    {
        if (!Hurting)
        {
            Movement();
        }
        SwitchAnim();
    }
    void Movement()
    {
        float horizontalmove;
        horizontalmove = Input.GetAxis("Horizontal");

        float facedirection = Input.GetAxisRaw("Horizontal");

        //Playermovement
        if (horizontalmove != 0)
        {
            rb.velocity = new Vector2(horizontalmove * speed * Time.deltaTime, rb.velocity.y);
            anim.SetFloat("Running", Mathf.Abs(facedirection));
        }
        if (facedirection != 0)
        {
            transform.localScale = new Vector3(facedirection, 1, 1);
        }
        //Jump
        if (Input.GetButtonDown("Jump") && coll.IsTouchingLayers(ground))
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpforce * Time.deltaTime);
            anim.SetBool("Jumping", true);
        }
    }

    //Animation
    void SwitchAnim()
    {
        anim.SetBool("idle", false);
        if (anim.GetBool("Jumping"))
        {
            if (rb.velocity.y < 0)
            {
                anim.SetBool("Jumping", false);
                anim.SetBool("Falling", true);
            }
        }
        else if(Hurting)
        {
            anim.SetBool("hurt", true);
            if (Mathf.Abs(rb.velocity.x) < 3f)
            {
                anim.SetBool("hurt", false);
                anim.SetBool("idle", true);
                Hurting = false;
            }
        }
        else if (coll.IsTouchingLayers(ground))
        {
            anim.SetBool("Falling", false);
            anim.SetBool("idle", true);
        }
    }

    //Collectcherry
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "collection")
        {
            Destroy(collision.gameObject);
            Cherry += 1;
            Cherrynum.text = Cherry.ToString();
        }
    }
    //destroy
    private void OnCollisionEnter2D(Collision2D collision)
    {
            if (collision.gameObject.tag == "enemy")
            {
            frog newfrog = collision.gameObject.GetComponent<frog>();
                if (anim.GetBool("Falling"))
            {
                newfrog.JumpOn();
            }
                rb.velocity = new Vector2(rb.velocity.x, jumpforce * Time.deltaTime);
                anim.SetBool("Jumping", true);
            }
          // else if(transform.position.x < collision.gameObject.transform.position.x)
           // {
           //     rb.velocity = new Vector2(-8, rb.velocity.y);
           //     Hurting = true;
           // }
          //if (transform.position.x > collision.gameObject.transform.position.x)
          // {
          //     rb.velocity = new Vector2(8, rb.velocity.y);
          //      Hurting = true;
           //}
        }
    }

